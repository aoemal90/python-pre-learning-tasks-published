def factors(number):
    factors_list = []

    for i in range(1, number + 1):
        if number % i == 0 and i != 1 and i != number:
            factors_list.append(i)

    if len(factors_list) != 0:
        return factors_list
    else:
        return f"{number} is a prime number"


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
