def vowel_swapper(string):
    sub_strings = {'a': '4', 'e': '3', 'i': '!', 'o': 'ooo', 'u': '|_|',
                   'A': '4', 'E': '3', 'I': '!', 'O': '000', 'U': '|_|'}

    new_string = ""

    for i in range(len(string)):
        letter = string[i]
        if letter in sub_strings:

            index = -1
            try:  # Attempt to find the index of second occurrence, ignoring case of the string
                index = string.casefold().index(letter.casefold(), string.casefold().index(letter.casefold())+1)
            except ValueError:
                pass

            if index == i:
                letter = sub_strings[letter]

        new_string += letter

    return new_string


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
