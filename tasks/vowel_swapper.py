def vowel_swapper(string):
    sub_strings = {'a': '4', 'e': '3', 'i': '!', 'o': 'ooo', 'u': '|_|',
                   'A': '4', 'E': '3', 'I': '!', 'O': '000', 'U': '|_|'}

    new_string = ""

    for i in string:
        if i in sub_strings:
            i = sub_strings[i]
        new_string += i

    return new_string


print(vowel_swapper("aA eE iI oO uU"))  # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World"))  # Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console


# for _ in range(10000):
#     vowel_swapper("aA eE iI oO uU")
#     vowel_swapper("Hello World")
#     vowel_swapper("Everything's Available")
